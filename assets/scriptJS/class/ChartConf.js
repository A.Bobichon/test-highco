class ChartConfLine {
    constructor() {
        this.labels = [];
        this.data = {
            labels: [],
            datasets: [{
                label: '',
                backgroundColor: '',
                borderColor: '',
                data: []
            }]
        };
        this.config = {
            type: 'line',
            data: {},
            options: {}
        };
    }

    SetConfLine(
        adscisse,
        txtLigne,
        backgroundColor,
        borderColor,
        ordonne
    ) {
        // Set labels
        this.labels.push(...adscisse);

        // Set data
        this.data.labels = this.labels;
        this.data.datasets[0].label = txtLigne;
        this.data.datasets[0].backgroundColor = backgroundColor;
        this.data.datasets[0].borderColor = borderColor;
        this.data.datasets[0].data.push(...ordonne);

        // Set config
        this.config.data = this.data;

        return this.config;
    }


}

class ChartConfPie {
    constructor() {
        this.data = {
            labels: [],
            datasets: [{
                label: '',
                data: [],
                backgroundColor: [],
                hoverOffset: 3
            }]
        };
        this.config = {
            type: 'doughnut',
            data: {}
        };
    }

    SetConfPie(
        codePostal,
        txtLigne,
        nbCodePostal,
        postalColor
    ) {

        // Set data
        this.data.labels.push(...codePostal);
        this.data.datasets[0].label = txtLigne;
        this.data.datasets[0].data.push(...nbCodePostal);
        this.data.datasets[0].backgroundColor.push(...postalColor);

        // Set config
        this.config.data = this.data;

        return this.config;
    }

}

export { ChartConfLine, ChartConfPie };