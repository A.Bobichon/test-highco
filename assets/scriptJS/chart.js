import { ChartConfLine, ChartConfPie } from './class/ChartConf.js';

function genColor(nbColor) {
    let arrayColor = [];

    for (let index = 0; index < nbColor; index++) {
        arrayColor.push('#' + (Math.random() * 0xFFFFFF << 0).toString(16));
    }

    return arrayColor;
}

/**
 * BartChart
 */
const sectionBartChart = document.getElementById('bartChartSec');
const dataJSONBart = sectionBartChart.getAttribute('data-chart');
const dataBartParsed = JSON.parse(dataJSONBart);
const BartChartConf = new ChartConfLine();

let abscisse = [];
let ordonne = [];

for (const data of dataBartParsed) {
    abscisse.push(data["MontantTotal"]);
    ordonne.push(data["NbUniqueClient"]);
}

let BartChartConfReady = BartChartConf.SetConfLine(
    abscisse,
    'NbUniqueClient/MontantTotal',
    'white',
    'grey',
    ordonne
);

const BartChart = new Chart(
    document.getElementById('canvasBartChart'),
    BartChartConfReady
);


/**
 * PieChart
 */
const sectionPieChart = document.getElementById('chartPietSec');
const dataJSONPie = sectionPieChart.getAttribute('data-chart');
const dataPieParsed = JSON.parse(dataJSONPie);
const BartPieConf = new ChartConfPie();

let codePostaux = [];
let nbCodePostaux = [];
let colors = genColor(10);

for (const data of dataPieParsed) {
    codePostaux.push(data["CodePostal"] + " CP");
    nbCodePostaux.push(data["NbDePartement"]);
}

let PieConfReady = BartPieConf.SetConfPie(
    codePostaux,
    'NbCodePostaux/CodePostaux',
    nbCodePostaux, [...colors]
);

const PieChart = new Chart(
    document.getElementById('canvasPietChart'),
    PieConfReady
);