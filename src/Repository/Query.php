<?php 

namespace App\Repository;

Class Query{

    /**
     * Query Create table
     */
    public function CreateTableStape1(){
        return "   
            CREATE TABLE if not exists TelMontant (
                id int NOT NULL AUTO_INCREMENT,
                Tel int NOT NULL,
                MontantTotal int,
                PRIMARY KEY (id),
                UNIQUE KEY uniqueTelMontant (Tel)
            );
        ";
        
    }

    public function PostStape1($client){

        return "
            INSERT INTO TelMontant(Tel, MontantTotal) 
            VALUES($client->tel, $client->montant)
            ON DUPLICATE KEY UPDATE MontantTotal = MontantTotal + $client->montant;
        ";
    }

    /**
     * Query Insert 
     */
    public function CreateTableStape2(){ 
        
        return "   
            CREATE TABLE if not exists TelAndCode (
                id int NOT NULL AUTO_INCREMENT,
                Tel int NOT NULL,
                CodePostal int NOT NULL,
                PRIMARY KEY (id),
                UNIQUE KEY uniqueTelCode(Tel)
            );

            CREATE UNIQUE INDEX indexUniqueTelCodePostal2 ON TelAndCode( Tel, CodePostal);
        ";
    }

    public function PostStape2($client){
        return "
            INSERT INTO TelAndCode (Tel, CodePostal)
            VALUES ($client->tel, $client->codePostal);
        ";
    }

    /**
     * Query Select for chart
     */
    public function GetDataForBarChart(){
        return "
            SELECT MontantTotal, Count(MontantTotal) AS NbUniqueClient FROM TelMontant GROUP BY MontantTotal ORDER BY NbUniqueClient;
        ";
    }

    public function GetDataForPieChart(){
        return "
            SELECT count(CodePostal) as NbDePartement, CodePostal FROM TelAndCode GROUP BY CodePostal ORDER BY NbDePartement DESC LIMIT 10;     
        ";
    }

    
}