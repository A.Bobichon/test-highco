<?php

namespace App\class_settings;

namespace App\class_settings;

class Client {
    public function __construct($date, $montant, $tel, $codePostal)
    {
        $this->date = $date;
        $this->montant = $montant;
        $this->tel = $tel;
        $this->codePostal = $codePostal;
    }
}
