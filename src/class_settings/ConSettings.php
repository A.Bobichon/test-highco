<?php

namespace App\class_settings;

use PDO;
use PDOException;

class ConSettings {
    public function __construct()
    {
        $this->user = $_ENV['USER_SQL'];
        $this->password = $_ENV['PASSWORD'];
        $this->host = $_ENV['HOST'];
        $this->port = $_ENV['PORT'];
        $this->bdd = $_ENV['BDD'];
    }

    function OpenCon()
    {

        $conn = null;
        $dsn = "mysql:host=$this->host;dbname=$this->bdd";

        try{
            $conn = new PDO("mysql:$dsn", "$this->user", "$this->password");
        }
        catch(PDOException $e){
            $conn = $e->getMessage();
        }

        return $conn;
    }
    
    function QueryToExect ($conn, $sql){
        return $conn->query($sql);
    }


}
