<?php

namespace App\Service;

Class BDDActionService {


    public function CreateTableAndImport($query, $queryCreateTable,  $queryPost,$conn, $dataRefacto){

        /**
         *  Create the bdd
         */
        $conn->exec($query->{$queryCreateTable}());

        foreach( $dataRefacto as $sendData){

            /**
             * exec the import
             */
            $import = $query->{$queryPost}($sendData);
            $conn->exec($import);
            
        }
    }

    public function GetTableData($conn, $query, $queryGet){

        //$export = $query->{$queryGet}();
        $request = $conn->query($query->{$queryGet}());
        $result = $request->fetchAll();

        return $result;
    }
    
}