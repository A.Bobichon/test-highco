<?php

namespace App\Service;

use App\class_settings\Client;

Class CSVLecteurService {
    public function __construct($path)
    {
        $this->path = "../$path";
        $this->dataToTransform = [];
        $this->refactoData = [];
    }

    public function GetDataCSV(){
        
        // Read the file and set the data
        $data = fopen($this->path,"r"); 

        while(! feof($data))
        {

            $line = fgetcsv($data, null, ";");
            array_push($this->dataToTransform, $line);
        }

        fclose($data);

        $data = fopen($this->path, "w");
        fwrite($data, "", 0);
        fclose($data);

        return $this->dataToTransform;
    }

    public function SetDataClient($dataToTransform){

        foreach( $dataToTransform as $data){
            array_push($this->refactoData, new Client($data[0], $data[1], $data[2], $data[3]));
        }

        return $this->refactoData;
    }
    
}