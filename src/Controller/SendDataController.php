<?php


namespace App\Controller;

use App\Service\BDDActionService;
use App\class_settings\ConSettings;
use App\Repository\Query;
use App\Service\CSVLecteurService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

Class SendDataController extends AbstractController {

    #[Route('/Dashboard')]
    public function init(){

        $settings = new ConSettings();
        $csvLecteur = new CSVLecteurService('test.csv');
        $query = new Query();
        $BDDService = new BDDActionService();

        /**
         * Read the CSV 
         */
        $dataToTransform = $csvLecteur->GetDataCSV('test.csv');
        $dataRefacto = $csvLecteur->SetDataClient($dataToTransform);
        $conn = $settings->OpenCon();

        /**
         * Create and Import Data
         */
        $BDDService->CreateTableAndImport($query, 'CreateTableStape1', 'PostStape1', $conn, $dataRefacto);
        $BDDService->CreateTableAndImport($query, 'CreateTableStape2', 'PostStape2', $conn, $dataRefacto);

        /**
         * Get all Data for chart
         */
        $dataBarChart = $BDDService->GetTableData($conn, $query, 'GetDataForBarChart');
        $dataPieChart = $BDDService->GetTableData($conn, $query, "GetDataForPieChart");

        return $this->render('/dashboard.html.twig', [
            "titleToSend" => "TEST TECHNIQUE",
            "dataBarChart" => json_encode($dataBarChart),
            "dataPieChart" => json_encode($dataPieChart)
        ]);
    }
} 

