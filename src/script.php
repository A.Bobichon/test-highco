<?php

use App\class_settings\ConSettings;
use App\Repository\Query;
use App\Service\CSVLecteur;

$settings = new ConSettings();
$csvLecteur = new CSVLecteur('test.csv');
$query = new Query();


/**
 * Init the bdd creation
 */
$queryCreateTableStape1 = $query->CreateTableStape1();
$queryCreateTableStape2 = $query->CreateTableStape2();
$conn = $settings->OpenCon();

/**
 *  Create the bdd
 */
$conn->exec($queryCreateTableStape1);
$conn->exec($queryCreateTableStape2);

/**
 * Read the CSV 
 */
$dataToTransform = $csvLecteur->GetDataCSV('test.csv');
$dataRefacto = $csvLecteur->SetDataClient($dataToTransform);

/**
 * Inport data
 */

 foreach( $dataRefacto as $sendData){

    /**
     * exec the stape
     */
    $import1 = $query->PostStape1($sendData);
    $conn->exec($import1);
    
    /**
     * exec the stape 2 
     */
    $import2 = $query->PostStape2($sendData);
    $conn->exec($import2);
 }


